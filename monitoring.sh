#!/bin/bash
#echo -e "\t#Architecture : $(uname -a)";
arch=$(uname -a)

#echo -e "\t#CPU physical : $(nproc --all)";
#echo -e "\t#vCPU : $(cat /proc/cpuinfo | grep processor | wc -l)";
#echo -e "\t#CPU physical : $(grep "physical id" /proc/cpuinfo | sort | uniq | wc -l)";
#echo -e "\t#vCPU : $(grep "^processor" /proc/cpuinfo | wc -l)";
cpu=$(grep "physical id" /proc/cpuinfo | sort | uniq | wc -l)
vcpu=$(grep "^processor" /proc/cpuinfo | wc -l)

#echo -ne "\t#Memory Usage: $(free -t --mega | awk 'NR == 2 {print $3}')/";
#echo -ne "$(free -t --mega | awk 'NR == 2 {print $2}')MB ";
#echo -e "($(free -t | awk 'NR == 2 {printf("%.2f%"), $3/$2*100}'))";
memu=$(free -t --mega | awk 'NR == 2 {print $3}')
memt=$(free -t --mega | awk 'NR == 2 {print $2}')
memp=$(free -t | awk 'NR == 2 {printf("%.2f"), $3/$2*100}')

#echo -ne "\t#Disk Usage: $(df -Bm | grep /dev | grep LVMGroup | awk '{used += $3} END {print used}')/";
#echo -ne "$(df -Bg | grep /dev | grep LVMGroup | awk '{total += $2} END {print total}')Gb";
#echo -e " ($(df -Bm | grep /dev | grep LVMGroup | awk '{total += $2} {used += $3} END {printf("%.0f", used/total * 100)}')%)";
disku=$(df -Bm | grep /dev | grep LVMGroup | awk '{used += $3} END {print used}')
diskt=$(df -Bg | grep /dev | grep LVMGroup | awk '{total += $2} END {print total}')
diskp=$(df -Bm | grep /dev | grep LVMGroup | awk '{total += $2} {used += $3} END {printf("%.0f", used/total * 100)}')

#echo -e "\t#CPU load: $(top -bn1 | grep "%Cpu(s)" | cut -d "," -f4 | awk '{printf("%.1f", 100 - $1)}')%";
cpup=$(top -bn1 | grep "%Cpu(s)" | cut -d "," -f4 | awk '{printf("%.1f", 100 - $1)}')

#echo -e "\t#Last boot: $(who -b | awk '{print $3 " " $4}')"
boot=$(who -b | awk '{print $3 " " $4}')

#echo -ne "\t#LVM use: "
if [ $(lsblk | grep "lvm" | wc -l) -gt 0 ]; then
    lvm="yes" 
else
    lvm="no"
fi

#echo -e "\t#Connexions TCP : $(netstat -t | grep "ESTABLISHED" | wc -l) ESTABLISHED"
#echo -e "\t#User log: $(users | wc -w)"
tcp=$(netstat -t | grep "ESTABLISHED" | wc -l)
usern=$(users | wc -w)

#echo -e "\t#Network: IP $(hostname -I | xargs) ($(/sbin/ifconfig -a | grep "ether " | awk '{print $2}'))"
#echo -e "\t#Sudo : $(journalctl _COMM=sudo | grep COMMAND | wc -l) cmd"
ip=$(hostname -I | xargs)
mac=$(/sbin/ifconfig -a | grep "ether " | awk '{print $2}')
sudocmd=$(journalctl _COMM=sudo | grep COMMAND | wc -l)

wall "	#Architecture: $arch
	#CPU physical : $cpu
	#vCPU : $vcpu
	#Memory Usage: $memu/$(echo $memt)MB ($memp%)
	#Disk Usage: $disku/$(echo $diskt)Gb ($diskp%)
	#CPU load: $cpup%
	#Last boot: $boot
	#LVM use: $lvm
	#Connexions TCP : $tcp ESTABLISHED
	#User log: $usern
	#Network: IP $ip ($mac)
	#sudo : $sudocmd cmd"
